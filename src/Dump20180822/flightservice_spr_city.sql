-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: flightservice
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `spr_city`
--

DROP TABLE IF EXISTS `spr_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `spr_city` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_en` varchar(45) DEFAULT NULL,
  `name_ru` varchar(45) DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `flight_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spr_city`
--

LOCK TABLES `spr_city` WRITE;
/*!40000 ALTER TABLE `spr_city` DISABLE KEYS */;
INSERT INTO `spr_city` VALUES (1,'Vena','Вена',1,NULL),(2,'Berlin','Берлин',2,NULL),(3,'Munich','Мюнхен',2,NULL),(4,'Dortmund','Дортмунд',2,NULL),(5,'Dusseldorf','Дюссельдорф',2,NULL),(6,'Hamburg','Гамбург',2,NULL),(7,'Leverkusen','Леверкузен',2,NULL),(8,'Budapest','Будапешт',3,NULL),(9,'Stockholm','Стокгольм',4,NULL),(10,'Oslo','Осло',5,NULL),(11,'Helsinki','Хельсинки',6,NULL),(12,'London','Лондон',7,NULL),(13,'Newcastle','Ньюкасл',7,NULL),(14,'Liverpool','Ливерпуль',7,NULL),(15,'Manchester','Манчестер',7,NULL),(16,'Southhampton','Саутгемптон',7,NULL),(17,'Liester City','Лестер Сити',7,NULL),(18,'Glasgow','Глазго',8,NULL),(19,'Moscow','Москва',9,NULL),(20,'Saint-Petersburg','Санкт-Петербург',9,NULL),(21,'Kazan','Казань',9,NULL),(22,'Minsk','Минск',10,NULL),(23,'Vilnius','Вильнюс',11,NULL),(24,'Kaunas','Каунас',11,NULL),(25,'Riga','Рига',12,NULL),(26,'Warsaw','Варшава',13,NULL),(27,'Krakow','Краков',13,NULL),(28,'Wroclaw','Вроцлав',13,NULL),(29,'New York City','Нью-Йорк',14,NULL),(30,'Washington','Вашингтон',14,NULL),(31,'Boston','Бостон',14,NULL),(32,'Atlantic City','Атлантик Сити',14,NULL),(33,'Chicago','Чикаго',14,NULL),(34,'Los Angeles','Лос-Анджелес',14,NULL),(35,'Paris','Париж',15,NULL),(36,'Monaco','Монако',15,NULL),(37,'Marseille','Марсель',15,NULL),(38,'Madrid','Мадрид',16,NULL),(39,'Barcelona','Барселона',16,NULL),(40,'Valencia','Валенсия',16,NULL),(41,'Sevilla','Севилья',16,NULL),(42,'Bilbao','Бильбао',16,NULL),(43,'Lisbon','Лиссабон',17,NULL),(44,'Porto','Порту',17,NULL);
/*!40000 ALTER TABLE `spr_city` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-22 18:45:26
