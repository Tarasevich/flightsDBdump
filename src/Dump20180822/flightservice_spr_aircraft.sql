-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: flightservice
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `spr_aircraft`
--

DROP TABLE IF EXISTS `spr_aircraft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `spr_aircraft` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `number` varchar(10) DEFAULT NULL,
  `place_count` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `place_bis` int(11) DEFAULT NULL,
  `place_eco_comf` int(11) DEFAULT NULL,
  `place_eco` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `number_UNIQUE` (`number`),
  KEY `companyFK_idx` (`company_id`),
  CONSTRAINT `companyFK` FOREIGN KEY (`company_id`) REFERENCES `spr_aircraft` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spr_aircraft`
--

LOCK TABLES `spr_aircraft` WRITE;
/*!40000 ALTER TABLE `spr_aircraft` DISABLE KEYS */;
INSERT INTO `spr_aircraft` VALUES (1,'Boing 777	','AQ1832-1',168,1,22,26,120),(2,'Boing 777	','TR1143-4',168,1,22,26,120),(3,'Boing 777	','YU5412-5',168,2,22,26,120),(4,'Boing 777	','IO3212-9',168,2,22,26,120),(5,'Boing 777	','JH5423-1',168,3,22,26,120),(6,'Boing 777	','JY8734-0',168,3,22,26,120),(7,'Boing 777	','UQ1231-2',168,4,22,26,120),(8,'Boing 777	','GH5643-4',168,4,22,26,120),(9,'Boing 777	','JK1243-8',168,4,22,26,120),(10,'Boing 777	','OZ6634-5',168,5,22,26,120),(11,'Boing 777	','IQ6524-2',168,5,22,26,120),(12,'Boing 777	','PP4432-5',168,5,22,26,120),(13,'Boing 777	','JJ7812-4',168,5,22,26,120),(14,'Boing 777	','QA5423-7',168,6,22,26,120),(15,'Boing 777	','NM3199-1',168,6,22,26,120),(16,'Boing 777	','YT1354-2',168,7,22,26,120),(17,'Boing 777	','IY4231-1',168,8,22,26,120),(18,'Boing 777	','KL4763-2',168,9,22,26,120),(19,'Boing 777	','GR6734-4',168,1,22,26,120),(20,'Boing 777	','OU5423-5',168,1,22,26,120),(21,'Boing 777	','XC4376-3',168,3,22,26,120),(22,'Boing 777	','GF5673-5',168,8,22,26,120),(23,'Boing 777','XV3454-4',168,10,22,26,120),(24,'Boing 777','QW3454-2',168,10,22,26,120),(25,'Airbus A319','CV4365-2',319,1,30,110,179),(26,'Airbus A319','GW5672-5',319,3,30,110,179),(27,'Airbus A319','OK4242-4',319,4,30,110,179),(28,'Airbus A319','AZ4365-0',319,5,30,110,179),(29,'Airbus A319','KL5498-3',319,6,30,110,179),(30,'Airbus A319','OP8788-5',319,8,30,110,179),(31,'Airbus A319','KL5476-7',319,9,30,110,179),(32,'Airbus A319','ZS4323-1',319,3,30,110,179),(33,'Airbus A319','TY4387-4',319,10,30,110,179),(34,'Airbus A319','OL3243-2',319,2,30,110,179),(35,'Airbus A319','GZ5413-4',319,4,30,110,179),(36,'Airbus A319','OP4376-5',319,7,30,110,179);
/*!40000 ALTER TABLE `spr_aircraft` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-22 18:45:26
